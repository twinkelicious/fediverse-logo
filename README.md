# Fediverse logo

A logo for Fediverse, a decentralised social media network, in the style of space agencies and fictional organisations from various science fiction franchises.             

![Fediverse Logo Cover Image](misc/fediverse-logo_cover_image.jpg "Fediverse Logo Cover Image")


## Description

In addition to the well-known [Fediverse logo](https://en.wikipedia.org/wiki/File:Fediverse_logo_proposal.svg) by Eukombos and others, I developed this logo as part of a project for [Digitalcourage](https://digitalcourage.de), which draws on the visual aesthetics of space agencies and various fictional organisations from different science fiction franchises, based on the name Fediverse (a portmanteau of Federated and Universe). The purpose of this logo is to support the Fediverse and to increase the visibility and recognition of the Fediverse through this logo and its use.

This is not an official logo and is intended as an additional alternative to the previously frequently used Fediverse logo to strengthen visual diversity.


## Variations

There are different variations of this logo to cover various application scenarios. Here is an overview of these variations:

<table>
<thead>
<tr>
<th width="25%">01 Main Logo</th>
<th width="25%">02 Variation inverted</th>
<th width="25%">03 Variation laurel wreath</th>
<th width="25%">04 Variation Fediverse Explorer</th></tr>
</thead>
<tbody>
<tr>
<td>![01 Main Logo](misc/preview_01_main_logo.png "01 Main Logo")</td>
<td>![02 Variation inverted](misc/preview_02_variation_inverted.png "02 Variation inverted")</td>
<td>![03 Variation laurel wreath](misc/preview_03_variation_laurel_wreath.png "03 Variation laurel wreath")</td>
<td>![04 Variation Fediverse Explorer](misc/preview_04_variation_fediverse_explorer.png "04 Variation Fediverse Explorer")</td>
</tr>
</tbody>
</table>


## License

> Fediverse Logo by [Jens Reimerdes](https://jens-reimerdes.de) is licensed under the [Creative Commons Attribution 4.0 License](https://creativecommons.org/licenses/by/4.0/) (CC BY 4.0).

You are free to share (copy and redistribute) and adapt (remix, transform and build upon) the material in any medium or format for any purpose, even commercially under the term of attribution. You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.


## Support and Merchandise

If you would like to support the work on this and similar projects, you are cordially invited to make a [donation to Digitalcourage](https://digitalcourage.de/spenden). Alternatively, you can also purchase one of the many [Fediverse merchandise items](https://shop.digitalcourage.de/themen/fediverse-artikel-merchandise/) in the Digitalcourage shop. The proceeds will be reinvested in the charitable work of Digitalcourage.